import { GetState } from 'zustand';
import { StateDraft, Store } from '../types';

const maxFishes = 10;

export const createFishSlice = (set: StateDraft, get: GetState<Store>) => ({
  fishes: maxFishes,
  repopulate: () => {
    set((draft) => {
      draft.fish.fishes = maxFishes;
    });
  },
  decreasePopulation: () =>
    set((draft) => {
      const fishes = draft.fish.fishes;
      draft.fish.fishes = fishes > 1 ? fishes - 1 : 0;
    }),
});
