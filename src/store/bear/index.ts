import { GetState } from 'zustand';

import { StateDraft, Store } from '../types';

export const createBearSlice = (set: StateDraft, get: GetState<Store>) => ({
  eatFish: () => get().fish.decreasePopulation(),
});
