import create from 'zustand';

import { devtools } from 'zustand/middleware';

import immer from './config';

import { createFishSlice } from './fish';
import { createBearSlice } from './bear';

import { Store } from './types';

const useStore = create<Store>(
  devtools(
    immer((set, get) => ({
      bear: createBearSlice(set, get),
      fish: createFishSlice(set, get),
    }))
  )
);

export default useStore;
