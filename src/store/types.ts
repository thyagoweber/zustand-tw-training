import { WritableDraft } from 'immer/dist/internal';

import { Bear } from '../types/Bear';
import { Fish } from '../types/Fish';

export type Store = {
  fish: Fish;
  bear: Bear;
};

export type StateDraft = (
  partial: Store | ((draft: WritableDraft<Store>) => void),
  replace?: boolean,
  name?: string
) => void;
