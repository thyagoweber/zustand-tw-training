import Home from '../components/pages/Home';

import './styles.css';

function App() {
  return (
    <div className="bg-white">
      <Home />;
    </div>
  );
}

export default App;
