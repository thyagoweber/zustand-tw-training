import Mountain from '../../organisms/Mountain';

const Home = (): JSX.Element => {
  return (
    <div className="container mx-auto font text-3xl">
      <Mountain />
    </div>
  );
};

export default Home;
