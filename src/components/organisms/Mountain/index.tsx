import bearEatingFish from '../../../assets/images/bear-eating-fish.jpg';
import useStore from '../../../store';

export default function Mountain() {
  const { fishes, eatFish, repopulate } = useStore(({ fish, bear }) => ({
    fishes: fish.fishes,
    eatFish: bear.eatFish,
    repopulate: fish.repopulate,
  }));

  return (
    <div className="text-center p-16">
      <div className="mb-16">
        <button className="bg-white" onClick={eatFish}>
          <img src={bearEatingFish} width="100%" />
        </button>
      </div>

      <div className="mb-8">
        <button className="text-danger" onClick={repopulate}>
          &#128260; Repopulate !
        </button>
      </div>

      <div className="mb-8 text-lg text-info">
        &#128031; Fish population : {fishes} &#128031;
      </div>
    </div>
  );
}
