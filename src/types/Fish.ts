export type Fish = {
  fishes: number;
  repopulate: () => void;
  decreasePopulation: () => void;
};
