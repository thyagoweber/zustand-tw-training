export type Bear = {
  eatFish: () => void;
};
